中文 ↓↓↓↓

[![firefox](https://img.shields.io/amo/v/big-search?style=flat-square&color=success)](https://addons.mozilla.org/firefox/addon/big-search/) [![chrome](https://img.shields.io/chrome-web-store/v/ojcnjeigmgjaiolalpapfnmmhdmpjhfb?style=flat-square&color=success)](https://chrome.google.com/webstore/detail/big-search/ojcnjeigmgjaiolalpapfnmmhdmpjhfb) 

Here is where browser addon [**Big Search**](https://github.com/garywill/BigSearch) downloaded / updated. 

You can [**install and update Big Search from Google Chrome Web Store directly**](https://chrome.google.com/webstore/detail/big-search/ojcnjeigmgjaiolalpapfnmmhdmpjhfb). 

Here is just an alternative place for downloading:

1. Please find the file `bigsearch-xxxx.crx` from the list to download.
2. In browser, click Menu -> (More Tools ->) Extensions. Enable "Developer mode" (and "Allow from other stores" if there is).
3. Drag the `.crx` file to drop it into the center of browser window

> Above instructions cover common browsers. If still meet problem, maybe we need a little search: [How to install crx](https://www.google.com/search?q=how+to+install+crx)

[**Big Search Source code / Read more about it / Feedback**](https://github.com/garywill/BigSearch) | [Change log](https://addons.mozilla.org/firefox/addon/big-search/versions/)

---

此处为浏览器扩展 [**大术专搜**](https://github.com/garywill/BigSearch) 下载/升级的地方。

您可以[从Chrome Web Store直接安装和更新大术专搜](https://chrome.google.com/webstore/detail/big-search/ojcnjeigmgjaiolalpapfnmmhdmpjhfb)。

这里只是一个备用的下载地址：

1. 请在文件列表中找到对应文件`bigsearch-xxxx.crx`下载

2. 在浏览器中，点击菜单->(更多工具->)扩展程序。打开“开发者选项”开关（和“允许来自其他商店”，如果有）。

3. 把`.crx`拖到浏览器中央

> 以上操作指南覆盖常见浏览器。若仍有问题，或许我们需要搜索一下：[如何安装crx](https://www.baidu.com/s?wd=%E5%A6%82%E4%BD%95%E5%AE%89%E8%A3%85crx)

[**大术专搜源代码 / 更多信息 / 反馈**](https://github.com/garywill/BigSearch) | [Change log](https://addons.mozilla.org/firefox/addon/big-search/versions/)

安装后接收更新：如果你的浏览器不连接Google Chrome Web Store更新（普通用户一般是这样），那么你可以到本扩展的设置中打开`Show when new version available`（有新版本时显示）开关。
